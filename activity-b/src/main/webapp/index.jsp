<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
	<head>
		<meta charset="ISO-8859-1">
		<title>Activity-B</title>
		<style>
			div{
				margin-top:5px; margin-bottom: 5px;
			}
			
			#container{
				display: flex; flex-direction: column;
			}
			
			div > input, div > select, div > textarea{
				
			}
		</style>
	</head>
	<body>
		<div id="container">
			<h1>Welcome to Servlet Job Finder!</h1>
			<form action="register" method="post">
				<div>
					<label for="fname">First Name</label>
					<input type="text" name="fname" required>
				</div>
				<div>
					<label for="lname">Last Name</label>
					<input type="text" name="lname" required>
				</div>
				<div>
					<label for="phone">Phone Number</label>
					<input type="tel" name="phone" required>
				</div>
				<div>
					<label for="email">Email Address</label>
					<input type="email" name="email" required>
				</div>
				
				<!-- App Discovery-->
				<fieldset>
					<legend>What platform did you find the registration?</legend>
					
					<input type="radio" id="friends" name="app_type" value="friends" required>
					<label for="friends">Friends</label><br>
					
					<input type="radio" id="social_media" name="app_type" value="socialmedia" required>
					<label for="social_media">Social Media</label><br>
					
					<input type="radio" id="others" name="app_type" value="others" required>
					<label for="others">Others</label>
				</fieldset>
				
				<div>
					<label for="date_of_birth">Date of Birth</label>
					<input type="date" name="date_of_birth" required>
				</div>
				
				<div>
					<label for="emp_app">Are you an employer or applicant?</label>
					<select id="person_type" name="emp_app">
						<option value="Applicant" selected="selected">Applicant</option>
						<option value="Employer">Employer</option>
					</select>
				</div>
				
				<!-- Special Instructions -->
				<div>
					<label for="description">Profile Description</label>
					<textarea maxlength="500" name="description">
					</textarea>
				</div>
				
				<!-- Button for Submitting -->
				<button>Register</button>
			</form>
		</div>
	</body>
</html>