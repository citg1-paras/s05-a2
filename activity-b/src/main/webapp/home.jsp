<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
	<head>
		<meta charset="ISO-8859-1">
		<title>Home Page</title>
	</head>
	<body>
		<%

			String userType = (String) session.getAttribute("userType");

			
			if (userType.equals("employer")) {
		%>
		<h1>Hello <%= session.getAttribute("fullName").toString() %>!</h1>
		<p>Welcome employer. You may now start browsing applicant profiles.</p>
		<%
			} else {
		%>
		<h1>Hello <%= session.getAttribute("fullName").toString() %>!</h1>
		<p>Welcome applicant. You may now start looking for your career opportunity.</p>
		<%
			}
		%>
		
	</body>
</html>