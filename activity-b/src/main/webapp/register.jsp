<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
	<head>
		<meta charset="ISO-8859-1">
		<title>Registration Confirmation</title>
	</head>
	<body>
		<form action="login" method="post">	
		<%
			String appType = session.getAttribute("appType").toString();
	
			if(appType.equals("friends")) {
				appType = "Friends";
			}
			else if(appType.equals("socialmedia")) {
				appType = "Social Media";
			}
			else{
				appType = "Others";
			}
		
			String dateOfBirth = session.getAttribute("dateOfBirth").toString();
			dateOfBirth = dateOfBirth.replace("T", " / ");
		%>
		<h1>Registration confirmation</h1>
		<p>First Name: <%= session.getAttribute("fname") %></p>
		<p>Last Name: <%= session.getAttribute("lname") %></p>
		<p>Phone: <%= session.getAttribute("phone") %></p>
		<p>Email: <%= session.getAttribute("email") %></p>
		<p>App Discovery: <%= appType %></p>
		<p>Date of Birth: <%= dateOfBirth %></p>
		<p>User Type: <%= session.getAttribute("userType") %></p>
		<p>Description: <%= session.getAttribute("description") %></p>
		
		
			<input type="submit">
		</form>
		
		<form action="index.jsp">
			<input type="submit" value="Back">
		</form>
	</body>
</html>