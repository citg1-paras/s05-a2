package com.zuitt;

import java.io.IOException;

import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;

@WebServlet("/register")
public class RegisterServlet extends HttpServlet {

	/**
	 * 
	 */
	private static final long serialVersionUID = 3834552629113201265L;

	public void init() throws ServletException {
		System.out.println("**************************************");
		System.out.println(" RegisterServlet has been initialized. ");
		System.out.println("**************************************");
	}
	
	public void doPost(HttpServletRequest req, HttpServletResponse res) throws IOException {
		String fname = req.getParameter("fname");
		String lname = req.getParameter("lname");
		String phone = req.getParameter("phone");
		String email = req.getParameter("email");
		String appType = req.getParameter("app_type");
		String dateOfBirth = req.getParameter("date_of_birth");
		String userType = req.getParameter("emp_app");
		String description = req.getParameter("description");
		
		HttpSession session = req.getSession();
		session.setAttribute("fname", fname);
		session.setAttribute("lname", lname);
		session.setAttribute("phone", phone);
		session.setAttribute("email", email);
		session.setAttribute("appType", appType);
		session.setAttribute("dateOfBirth", dateOfBirth);
		session.setAttribute("userType", userType);
		session.setAttribute("description", description);
		
		res.sendRedirect("register.jsp");
	}
	
	public void destroy() {
		System.out.println("**************************************");
		System.out.println(" RegisterServlet has been destroyed. ");
		System.out.println("**************************************");
	}
	
}
